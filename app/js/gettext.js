function getJSON(path) {
	return new Promise(function(resolve, reject) {
		var textRequest = new XMLHttpRequest();
		textRequest.open('GET', path);
		textRequest.onload = function() {
			if (textRequest.readyState == 4 &&
				textRequest.status >= 200 &&
				textRequest.status < 400) {
				var siteText = JSON.parse(textRequest.responseText);
				resolve(siteText);
			} else {
				reject(textRequest);
			}
		}

		textRequest.send();
	});
}

window.onload = function() {
	getJSON('/text.json')
	.then(function(dictionary) {
		var translateElements = document.querySelectorAll('.translate');

		Array.prototype.forEach.call(translateElements, function(el) {
			el.innerText = dictionary[el.getAttribute('data-key')];
		});

		// for (var i = 0; i < translateElements.length; i++) {
		// 	var el = translateElements[i];
		// 	el.innerText = dictionary[el.getAttribute('data-key')];
		// }
	})
	.catch(function() {
		alert('ERRRORRRR!!!');
	})
};