function validate(){

   var phone = document.forms["form"]["phone"].value;
   var email = document.forms["form"]["email"].value;
   
   if (email.length==0){
      document.getElementById("emailf").innerHTML="*данное поле обязательно для заполнения";
      return false;
   }
   else {
      document.getElementById("emailf").innerHTML="";
   }

   var at = email.indexOf("@");
   var dot = email.indexOf(".");
   
   if (at<1 || dot <1){
      document.getElementById("emailf").innerHTML="*email введен не верно";
      return false;
   }

   if (phone.length==0){
      document.getElementById("phonef").innerHTML="*данное поле обязательно для заполнения";
      return false;
   }
}