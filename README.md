Лабораторная работа 1-2:

1. Верстка + адаптивная вертка.

2. Валидация верстки - без ошибок.

3. Оптимизировать css на сайте. Использовал SCSS, разделил стили на 3 файла для мобильной, десктопной и версии для планшетов. gulp-cssnano, gulp-autoprefixer, gulp-sass.

4. Настроить оптимизацию для js. gulp-uglifyjs.

5. Оптимизировать изображения на сайте (сжатие без потери качества). gulp-imagemin, imagemin-pngquant.

6. В случае если есть мелкие изображения, создать спрайт.

7. Проверить код на валидность https://validator.w3.org/



Лабораторная работа 3:

1.Настроить сборку js в gulp. Настроить es-lintner. В Gulpfile создана задача Lint, я не включал ее в задачу сборки, запускаю отдельно в терминале.

2.Реализовать hamburger меню в мобильной версии сайта. Файл common.js.

3.Слайдер с анимацией fadeIn/fadeOut. Файл slider.js.

4.pop-up форма обратной связи. Файл popup.js.

5.Валидация формы. Файл validform.js.

6.Используя XMLHttpRequest, считывать текстовые данные из json на страницу. Реализовать промис-обертку вокруг XMLHttpRequest. Файл gettext.js и text.json.